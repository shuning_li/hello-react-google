import React from 'react';
import './App.less';
import Menu from "./Menu";
import Search from './Search';
import Footer from './Footer';

const App = () => {
  return (
    <div className="App">
      <Menu />
      <Search imgUrl="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png" />
      <Footer/>
    </div>
  );
};

export default App;