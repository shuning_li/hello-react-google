import React from 'react';
import {MdSearch, MdMic} from 'react-icons/md';
import Button from './Button';
import './Search.less';

const Search = props => {
  return (
    <section className="search">
      <header>
        <img src={props.imgUrl}
             alt="Google Logo"/>
      </header>
      <section className="search-main">
        <MdSearch className='search-icon'/>
        <input type="text"/>
        <MdMic className="voice-icon"/>
      </section>
      <section>
        <Button text="Google搜索"/>
        <Button text="手气不错"/>
      </section>
    </section>
  );
};

export default Search;