import React from 'react';
import "./Footer.less"

const Footer = () => {
  return (
    <footer>
      <section className="footer-left">
        <ul>
          <li>广告</li>
          <li>商务</li>
        </ul>
      </section>
      <section className="footer-right">
        <ul>
          <li>隐私权</li>
          <li>条款</li>
          <li>设置</li>
        </ul>
      </section>
    </footer>
  )
}

export default Footer;