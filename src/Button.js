import React from 'react';
import "./Button.less"

const Button = props => {
  return (
    <button className="search-button">{props.text}</button>
  )
}

export default Button;