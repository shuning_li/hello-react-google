import React from 'react';
import './Menu.less';
import {MdApps} from 'react-icons/md';

const Menu = () => {
  return (
    <nav className='menu'>
      <ul>
        <li>Gmail</li>
        <li>图片</li>
        <li><MdApps className="md-apps"/></li>
        <li className="profile">J</li>
      </ul>
    </nav>
  );
};

export default Menu;